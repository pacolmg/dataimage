<?php

namespace Greetik\DataimageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use UploadHandler;
use Greetik\DataimageBundle\Entity\Dataimage;
use Greetik\DataimageBundle\Form\Type\DataimageType;
use Symfony\Component\Form\FormError;

class DefaultController extends Controller
{

    public function indexAction($type, $id, $filetype='image'){
        //use the interface
            return $this->render('DataimageBundle:Dataimage:index.html.twig', array(
                     'configFiles'=>array('modifyAllow'=>true, 'id'=>$id, 'type'=>$type, 'filetype'=>$filetype)
                 ));        
    }

    
    /**
    * If the method is POST, upload a new image. If it's GET get all the images from the id pass by parameter
    * 
    * @param int $id is received by Get Request and it's the id of the item that the image is associated to
    * @param string $type is received by Get Request and it's the type of the item that the image is associated to
    * @return the data of the uploaded image, or a images array
    * @author Pacolmg
    */
    public function uploadAction($type, $id, $filetype='image'){
        if ($this->getRequest()->getMethod() == "POST") {
           return new Response(json_encode($this->get('dataimage.tools')->uploadImage($id, $type, $filetype)), 200, array('Content-Type'=>'application/json'));
        }else {
             if ($this->getRequest()->getMethod() == "GET") return new Response(json_encode($this->get('dataimage.tools')->getImages($id, $type, 1, $filetype)), 200, array('Content-Type'=>'application/json'));        
        }
         return new Response(json_encode(array('files'=>array())), 200, array('Content-Type'=>'application/json'));
    }
 
    /**
    * Delete an image
    * 
    * @param int $id It's the id of the image
    * @author Pacolmg
    */
    public function dropAction(){
        if ($this->getRequest()->getMethod() != "POST") return new Response(json_encode(array( 'error'=>'No puede hacer esta operación')), 200, array('Content-Type'=>'application/json'));
        if (!$this->getRequest()->get('id')) return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>'No se encontró la imagen')), 200, array('Content-Type'=>'application/json'));
        return new Response(json_encode($this->get('dataimage.tools')->dropImage($this->getRequest()->get('id'))), 200, array('Content-Type'=>'application/json'));         
    }
      

    /**
    * Show the edit image form
    * 
    * @author Pacolmg
    */
     public function modifyformAction()
     {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        if ($request->get('id')) $image=$em->getRepository('DataimageBundle:Dataimage')->findOneById($request->get('id'));
        else $image=new Dataimage();
         
        $newForm = $this->createForm(new DataimageType(), $image);

        return $this->render('DataimageBundle:Dataimage:insert.html.twig',array('new_form' => $newForm->createView(),'id' => $request->get('id'), 'linkfield'=>$image->getFiletype() == 'image' ));
     }
     
     
   /**
    * Edit the data of image
    * 
    * @param int $id is received by Get Request
    * @param Image $item is received by Post Request
    * @author Pacolmg
    */
     public function modifyAction(){
        $request = $this->getRequest();
        $item = $request->get('Dataimage');
        $em = $this->getDoctrine()->getManager();
        if (@$item['id'] !==null){
           $image = $em->getRepository('DataimageBundle:Dataimage')->findOneById($item['id']);
        }else return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>'No se encontró la imagen a modificar')), 200, array('Content-Type'=>'application/json'));

        $editForm = $this->createForm(new DataimageType(), $image);
        
        $editForm->bind($request);
        
        if ($editForm->isValid()) {
            $em->persist($image);
            $em->flush();
            return new Response(json_encode(array('errorCode'=>0)), 200, array('Content-Type'=>'application/json'));
        }else{
            $errors = $editForm->getErrorsAsString();
            return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>$errors)), 200, array('Content-Type'=>'application/json'));
        }
         return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>'Error Desconocido')), 200, array('Content-Type'=>'application/json'));
     }    
    
     
    /**
    * move the image
    * 
    * @param int $id is received by Get Request
    * @param Image $item is received by Post Request
    * @author Pacolmg
    */
     public function moveAction(){
        if (!$this->getRequest()->get('id')) return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>'No se encontró la imagen')), 200, array('Content-Type'=>'application/json'));
        return new Response(json_encode($this->get('dataimage.tools')->moveImage($this->getRequest()->get('id'), $this->getRequest()->get('newposition'))), 200, array('Content-Type'=>'application/json'));
     }
}
